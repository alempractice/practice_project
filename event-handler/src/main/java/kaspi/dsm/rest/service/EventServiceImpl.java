package kaspi.dsm.rest.service;

import java.io.IOException;
import java.net.UnknownHostException;
import kaspi.dsm.rest.configuration.DBConnector;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import kaspi.dsm.rest.model.Event;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

@Service("eventService")
@Transactional
public class EventServiceImpl implements EventService{
    
    public void createEvent(Event event) {
         try {
              Client conn= new DBConnector().createConnectToES();
              event.setCnt(1);
           IndexResponse responseUser = conn.prepareIndex("event", "docs")
                    .setSource(jsonBuilder()
                            .startObject()
                            .field("source", event.getSource())
                            .field("theme", event.getTheme())
                            .field("description", event.getDescription())
//                            .field("cnt", event.getCnt())
                            .endObject()
                    ).get();
           conn.close();
        } catch (UnknownHostException ex) {}
          catch (IOException ex) {}
    }
}
