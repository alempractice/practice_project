package kaspi.dsm.rest.model;

import java.util.Map;

public class Event {

	private int cnt;
	private String source;
        private String theme;
        private String description;
	
        public Event(){
            }
	
	public Event(String source, String theme, String description){
		this.cnt = 1;
		this.source = source;
		this.theme = theme;
		this.description = description;
	}
         public Event( Map<String, Object> source) {
        for (Map.Entry<String, Object> event : source.entrySet())
        {
            if (event.getKey().equals("source")) this.source= (String)event.getValue();
            if (event.getKey().equals("theme")) this.theme = (String)event.getValue();
            if (event.getKey().equals("description")) this.theme = (String)event.getValue();
        }
        this.cnt = 1;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = 1;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
	
        
	@Override
	public String toString() {
		return "Event [source=" + source + ", theme=" + theme + ", desc=" + description
				+ "]";
	}


}
