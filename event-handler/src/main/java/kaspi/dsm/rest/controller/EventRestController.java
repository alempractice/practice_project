package kaspi.dsm.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import kaspi.dsm.rest.model.Event;
import kaspi.dsm.rest.service.EventService;

@RestController
public class EventRestController {

    @Autowired
    EventService eventService;  //Service which will do all data retrieval/manipulation work

//-------------------Create a Event--------------------------------------------------------
    @RequestMapping(value = "/event/", method = RequestMethod.POST)
    public ResponseEntity<String> createEvent(@RequestBody Event event, UriComponentsBuilder ucBuilder) {

        eventService.createEvent(event);
        HttpHeaders headers = new HttpHeaders();

        return new ResponseEntity<String>("Ok", HttpStatus.CREATED);
    }

}
