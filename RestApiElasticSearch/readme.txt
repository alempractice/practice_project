���������� ������������
������� ������������
selectUser( GetResponse )
selectManyUsers ( QueryBuilder qb = QueryBuilders.matchAllQuery()+SearchResponse + Scroll - ������ ������� ������ ���, ����� ���������� ����� 10000 ������� )
createUser ( IndexResponse )
createManyUsersByOne ( IndexResponse in cycle)
createManyUsersByBULK ( BulkProcessor and there used IndexRequest in bulkProcessor.add method)) 
updateUser ( IndexResponse ) 
updateManyUsersByBulk ( like createManyUsersByBULK  )
deleteUserById ( DeleteResponse ) 
deleteManyUsersByOne ( DeleteResponse in cycle ) 
deleteManyUsersByBulk ( BulkProcessor and DeleteRequest in bulkProcessor.add method)

Here result of tests
--- exec-maven-plugin:1.2.1:exec (default-cli) @ RestApiElasticSearch ---
Testing createUser API---BEGIN---
Testing createUser API---END-----935msec
Testing deleteManyUsersByBylk API---BEGIN---1000000
Testing deleteManyUsersByBulk API---END-----6963msec
Testing createUser API---BEGIN---
Testing createUser API---END-----76msec
Testing createManyUsersByOne API---BEGIN---100
Testing createManyUsersByOne API---END-----964msec
Testing deleteManyUsersByBylk API---BEGIN---100
Testing deleteManyUsersByBulk API---END-----137msec
////////////////////////////////////////////
Testing createManyUsersByOne API---BEGIN---500
Testing createManyUsersByOne API---END-----3080msec
Testing deleteManyUsersByBylk API---BEGIN---500
Testing deleteManyUsersByBulk API---END-----136msec
Testing createManyUsersByOne API---BEGIN---500
Testing createManyUsersByOne API---END-----2716msec
Testing deleteManyUsersByOne API---BEGIN---500
Testing deleteManyUsersByOne API---END-----2757msec
////////////////////////////////////////////
Testing createManyUsersByOne API---BEGIN---1000
Testing createManyUsersByOne API---END-----5848msec
Testing deleteManyUsersByBylk API---BEGIN---1000
Testing deleteManyUsersByBulk API---END-----87msec
Testing createManyUsersByOne API---BEGIN---1000
Testing createManyUsersByOne API---END-----5997msec
Testing deleteManyUsersByOne API---BEGIN---1000
Testing deleteManyUsersByOne API---END-----5370msec
////////////////////////////////////////////
Testing createManyUsersByOne API---BEGIN---5000
Testing createManyUsersByOne API---END-----32388msec
Testing deleteManyUsersByBylk API---BEGIN---5000
Testing deleteManyUsersByBulk API---END-----72msec
Testing createManyUsersByOne API---BEGIN---5000
Testing createManyUsersByOne API---END-----30400msec
Testing deleteManyUsersByOne API---BEGIN---5000
Testing deleteManyUsersByOne API---END-----30072msec
////////////////////////////////////////////
Testing createManyUsersByBULK API---BEGIN---10000
Testing createManyUsersByBULK API---END-----261msec
Testing deleteManyUsersByBylk API---BEGIN---10000
Testing deleteManyUsersByBulk API---END-----254msec
////////////////////////////////////////////
Testing createManyUsersByBULK API---BEGIN---20000
Testing createManyUsersByBULK API---END-----359msec
Testing deleteManyUsersByBylk API---BEGIN---20000
Testing deleteManyUsersByBulk API---END-----200msec
Testing createManyUsersByBULK API---BEGIN---50000
Testing createManyUsersByBULK API---END-----1044msec
Testing deleteManyUsersByBylk API---BEGIN---50000
Testing deleteManyUsersByBulk API---END-----619msec
////////////////////////////////////////////
Testing createManyUsersByBULK API---BEGIN---100000
Testing createManyUsersByBULK API---END-----2111msec
Testing deleteManyUsersByBylk API---BEGIN---100000
Testing deleteManyUsersByBulk API---END-----960msec
////////////////////////////////////////////
Testing createManyUsersByBULK API---BEGIN---1000000
Testing createManyUsersByBULK API---END-----34832msec
Testing updateManyUsersByBylk API---BEGIN---100000
Testing updateManyUsersByBylk API---END-----4135msec
Testing updateManyUsersByBylk API---BEGIN---500000
Testing updateManyUsersByBylk API---END-----23625msec
Testing updateManyUsersByBylk API---BEGIN---1000000
Testing updateManyUsersByBylk API---END-----51250msec
////////////////////////////////////////////
Testing selectUser API---BEGIN---1000
Testing SelectUser API---END-----549msec
Testing SelectManyUsers API---BEGIN---100
Testing SelectManyUsers API---END-----266msec
Testing SelectManyUsers API---BEGIN---500
Testing SelectManyUsers API---END-----524msec
Testing SelectManyUsers API---BEGIN---1000
Testing SelectManyUsers API---END-----247msec
Testing SelectManyUsers API---BEGIN---5000
Testing SelectManyUsers API---END-----654msec
Testing SelectManyUsers API---BEGIN---10000
Testing SelectManyUsers API---END-----644msec
Testing SelectManyUsers API---BEGIN---20000
Testing SelectManyUsers API---END-----735msec
Testing SelectManyUsers API---BEGIN---50000
Testing SelectManyUsers API---END-----1257msec
////////////////////////////////////////////
------------------------------------------------------------------------
BUILD SUCCESS
------------------------------------------------------------------------
Total time: 04:14 min
Finished at: 2016-05-19T17:21:34+06:00
Final Memory: 10M/116M
----------------------------