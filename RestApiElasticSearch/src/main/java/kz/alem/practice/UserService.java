/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice;

import java.util.List;

/**
 *
 * @author whoiam
 */

public interface UserService {
    
    public User selectById(int id);
    
    public List<User> selectManyUsers(int count);
    
    public void insertUser(User user);
    
    public void insertManyUsersByOne(int count);
    
    public void insertManyUsersByBulk(int count);
    
    public void updateUser(User user);
    
    public void updateManyUsersByBulk(int count);

    public void deleteUserById(int id);
    
    public void deleteManyUsersByOne(int count);
    
    public void deleteManyUsersByBulk(int count);
    
    public boolean isUserExist(User user);
    
}
