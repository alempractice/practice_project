/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice.controller;

import java.util.List;
import kz.alem.practice.User;
import kz.alem.practice.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author whoiam
 */
@RestController
public class ElasticSearchRestController {
    @Autowired
    UserService userService;
    
    @RequestMapping(value = "/user/bulk/{count}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> selectManyUsers(@PathVariable("count") int count){
//        Logger 
        List<User> users = userService.selectManyUsers(count);
        if (users.isEmpty()){
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }
    
    //--Retrieve Single User //
    @RequestMapping(value="/user/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable("id") int id){
        User user = userService.selectById(id);
        if (user == null){
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<User>(user, HttpStatus.OK);
    }
    
    //--Create a user
    @RequestMapping(value="/user/", method=RequestMethod.POST)
    public ResponseEntity<Void> createUser(@RequestBody User user, UriComponentsBuilder ucBuilder){
        userService.insertUser(user);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/user/{id}").buildAndExpand(user.getId()).toUri());
      return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }
     //--Create Many user
    @RequestMapping(value="/user/one/{count}", method=RequestMethod.POST)
    public ResponseEntity<Void> createManyUserByOne(@PathVariable("count") int count){
        userService.insertManyUsersByOne(count);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    //--Create Many user By Bulk
    @RequestMapping(value="/user/bulk/{count}", method=RequestMethod.POST)
    public ResponseEntity<Void> createManyUserByBulk(@PathVariable("count") int count){
        userService.insertManyUsersByBulk(count);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    //------------------- Update a User --------------------------------------------------------
    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<User> updateUser(@PathVariable("id") int id, @RequestBody User user) {
       
        User currentUser = userService.selectById(id);
         
        if (currentUser==null) {
            System.out.println("User with id " + id + " not found");
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        currentUser.setName(user.getName());
        currentUser.setAge(user.getAge());
        userService.updateUser(currentUser);
        return new ResponseEntity<User>(currentUser, HttpStatus.OK);
    }
    
//    //--------------------Update all users ---------------------------------------------------
    @RequestMapping(value = "/user/bulk/{count}", method = RequestMethod.PUT)
    public ResponseEntity updateManyUsersByBulk(@PathVariable("count") int count) {
        userService.updateManyUsersByBulk(count);
        return new ResponseEntity(HttpStatus.OK);
    }
    
    //------------------- Delete a User --------------------------------------------------------
     
    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<User> deleteUser(@PathVariable("id") int id) {
        User user = userService.selectById(id);
        if (user == null) {
            return new ResponseEntity<User>(HttpStatus.NOT_FOUND);
        }
        userService.deleteUserById(id);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }
    //------------------- Delete Many Users by One-------------------------------------------
     
    @RequestMapping(value = "/user/one/{count}", method = RequestMethod.DELETE)
    public ResponseEntity deleteManyUsersByOne(@PathVariable("count") int count) {
        userService.deleteManyUsersByOne(count);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
    
    //------------------- Delete All Users by Bulk ------------------------------------------------
     
    @RequestMapping(value = "/user/bulk/{count}", method = RequestMethod.DELETE)
    public ResponseEntity deleteManyUsersByBulk(@PathVariable("count") int count) {
        userService.deleteManyUsersByBulk(count);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
