/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice.configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

/**
 *
 * @author whoiam
 */
public class DBConnector {
    public Client getElasticSearchConn() throws UnknownHostException{
        Settings settings = Settings.settingsBuilder().put("cluster.name","my-application").build();
          Client client = TransportClient.builder().settings(settings).build().addTransportAddress(
                    new InetSocketTransportAddress(InetAddress.getByName("192.168.13.56"), 9300));
          return client;
    }
}
