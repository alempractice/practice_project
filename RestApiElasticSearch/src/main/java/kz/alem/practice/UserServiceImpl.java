/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import kz.alem.practice.configuration.DBConnector;
import org.elasticsearch.action.bulk.BackoffPolicy;
import org.elasticsearch.action.bulk.BulkProcessor;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.ByteSizeUnit;
import org.elasticsearch.common.unit.ByteSizeValue;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

/**
 *
 * @author whoiam
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
    
    private DataSource dataSource;
    private JdbcTemplate jdbcTemplate;
    private static List<User> users;
   
    public void setDataSource(DataSource dataSource){
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public User selectById(int id) {
      
        List<User> users = new ArrayList<User>() {};
        Map<String,Object> mapUser;
        String name = "";
        int age = 0;
        
        try {
           Client client= new DBConnector().getElasticSearchConn();
        GetResponse responseUser = client.prepareGet("user", "external", String.valueOf(id)).get();
        
        mapUser = responseUser.getSourceAsMap();
        Boolean isNull = mapUser == null;
        client.close();
        if (!isNull) {
            System.out.println(mapUser);
            User user = new User(String.valueOf(id),mapUser);
            users.add(user);
        } 
        }catch (UnknownHostException ex) {
            ex.printStackTrace();
        }
        if (!users.isEmpty())  return users.get(0);
           return null;
    } 
    
    @Override
    public List<User> selectManyUsers(int count) {
        List<User> users = new ArrayList<User>() {};
        
        try {
            
           Client client= new DBConnector().getElasticSearchConn();
            QueryBuilder qb = QueryBuilders.matchAllQuery();
            SearchResponse scrollResp = client.prepareSearch("user").setTypes("external")
                .setScroll(new TimeValue(60000))
                .setQuery(qb)
                .setSize(count).execute().actionGet();

            for(SearchHit hit : scrollResp.getHits()){
                User user = new User(hit.getId(), hit.getSource());
                users.add(user);
            }
            client.close();
        } catch (UnknownHostException ex) {ex.printStackTrace();}
            return users;
    }
         	
    @Override
    public void insertUser(User user) {
    
        try {
              Client client= new DBConnector().getElasticSearchConn();
            
           IndexResponse responseUser = client.prepareIndex("user", "external", String.valueOf(user.getId()))
                    .setSource(jsonBuilder()
                            .startObject()
                            .field("name", user.getName())
                            .field("age", user.getAge())
                            .endObject()
                    ).get();
           client.close();
        } catch (UnknownHostException ex) {}
          catch (IOException ex) {}
    }
    @Override
    public void insertManyUsersByOne(int count) {
          try {
             Client client= new DBConnector().getElasticSearchConn();
           for (int i=1; i <= count; i++)
           {
           IndexResponse responseUser = client.prepareIndex("user", "external", String.valueOf(i))
                    .setSource(jsonBuilder()
                            .startObject()
                            .field("name", "Smith_"+i)
                            .field("age", i)
                            .endObject()
                    ).get();
           }
           client.close();
        } catch (UnknownHostException ex) {}
        catch (IOException ex) {}
    }
    
    @Override
    public void insertManyUsersByBulk(int count) {
           try {
              Client client= new DBConnector().getElasticSearchConn();
        BulkProcessor bulkProcessor = BulkProcessor.builder(
        client,  
        new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId,
                                   BulkRequest request) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  BulkResponse response) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  Throwable failure) {} 
        })
        .setBulkActions(10000) 
        .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB)) 
        .setFlushInterval(TimeValue.timeValueSeconds(10)) 
        .setConcurrentRequests(2) 
        .setBackoffPolicy(
            BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)) 
        .build();
        for (int i=1; i<=count; i++)
        {   bulkProcessor.add(new IndexRequest("user", "external", String.valueOf(i))
            .source(jsonBuilder()
            .startObject()
                .field("name", "InsBulk Smith_"+i)
                .field("age", i)
            .endObject()));
        }
        bulkProcessor.close();
        client.close();
        } catch (UnknownHostException ex) {}
        catch (IOException ex) {}
    }
    
    @Override
    public void updateUser(User user) {
         try {
            Client client= new DBConnector().getElasticSearchConn();
            
           IndexResponse responseUser = client.prepareIndex("user", "external", String.valueOf(user.getId()))
                    .setSource(jsonBuilder()
                            .startObject()
                            .field("name", user.getName())
                            .field("age", user.getAge())
                            .endObject()
                    ).get();
           client.close();
        } catch (UnknownHostException ex) {}
          catch (IOException ex) {}
    }
    
    @Override
    public void updateManyUsersByBulk(int count) {
         try {
            Client client= new DBConnector().getElasticSearchConn();
        BulkProcessor bulkProcessor = BulkProcessor.builder(
        client,  
        new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId,
                                   BulkRequest request) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  BulkResponse response) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  Throwable failure) {} 
        })
        .setBulkActions(10000) 
        .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB)) 
        .setFlushInterval(TimeValue.timeValueSeconds(10)) 
        .setConcurrentRequests(2) 
        .setBackoffPolicy(
            BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)) 
        .build();
        for (int i=1; i<=count; i++)
        {   
            bulkProcessor.add(new IndexRequest("user", "external", String.valueOf(i))
            .source(jsonBuilder()
            .startObject()
                .field("name", "UpdBulk Smith_"+i)
                .field("age", i)
            .endObject()));
        }
        bulkProcessor.close();
        client.close();
        } catch (UnknownHostException ex) {}
        catch (IOException ex) {}
    }
    
    @Override
    public void deleteUserById(int id) {
        try {
             Client client= new DBConnector().getElasticSearchConn();
            DeleteResponse responseDelete = client.prepareDelete("user", "external", String.valueOf(id)).get();
            client.close();
        } catch (UnknownHostException unknownHostException) {
        }
    }
    @Override
    public void deleteManyUsersByOne(int count){
        try {
              Client client= new DBConnector().getElasticSearchConn();
            for (int i=0;i<=count;i++)
            {
            DeleteResponse responseDelete = client.prepareDelete("user", "external", String.valueOf(i)).get();
            }
            client.close();
        } catch (UnknownHostException unknownHostException) {
        }
    
    }
    @Override
    public void deleteManyUsersByBulk(int count) {
        try {
             Client client= new DBConnector().getElasticSearchConn();
        BulkProcessor bulkProcessor = BulkProcessor.builder(
        client,  
        new BulkProcessor.Listener() {
            @Override
            public void beforeBulk(long executionId,
                                   BulkRequest request) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  BulkResponse response) { } 

            @Override
            public void afterBulk(long executionId,
                                  BulkRequest request,
                                  Throwable failure) {} 
        })
        .setBulkActions(10000) 
        .setBulkSize(new ByteSizeValue(1, ByteSizeUnit.GB)) 
        .setFlushInterval(TimeValue.timeValueSeconds(10)) 
        .setConcurrentRequests(2) 
        .setBackoffPolicy(
            BackoffPolicy.exponentialBackoff(TimeValue.timeValueMillis(100), 3)) 
        .build();
        for (int i=1; i<=count; i++)
        {   
            bulkProcessor.add(new DeleteRequest("user", "external", String.valueOf(i)));
        }
        bulkProcessor.close();
    client.close();
  } catch (UnknownHostException ex) {}
          catch (IOException ex) {}
    }

    @Override
    public boolean isUserExist(User user) {
        return selectById(user.getId())!=null;
    }
}
