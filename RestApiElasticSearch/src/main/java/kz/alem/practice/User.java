/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice;

import java.util.Map;

/**
 *
 * @author whoiam
 */
public class User {
    private int id;
    private String name;
    private int age;
    
    public User(){
    }
     
    public User(int id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }

   

    public User(String id, Map<String, Object> source) {
        for (Map.Entry<String, Object> user : source.entrySet())
        {
            if (user.getKey().equals("name")) this.name= (String)user.getValue();
            if (user.getKey().equals("age")) this.age= (int)user.getValue();
        }
        this.id = Integer.valueOf(id);
    }
     
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public int getAge() {
        return age;
    }
 
    public void setAge(int age) {
        this.age = age;
    }
 
    @Override
    public String toString() {
        return "User [id=" + id + ", name=" + name.trim() + ", age=" + age + "]";
    }
 
 
}