/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kz.alem.practice;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kz.alem.practice.User;
import kz.alem.practice.User;
import kz.alem.practice.User;
import kz.alem.practice.UserServiceImpl;
import kz.alem.practice.UserServiceImpl;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author whoiam
 */
public class SpringRestTestClient {
    public static final String REST_SERVICE_URI = "http://localhost:8080/RestApiElasticSearch";
     
    /* GET */
    @SuppressWarnings("unchecked")
    private static void selectManyUsers(int count){
        System.out.println("Testing SelectManyUsers API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        List<User> usersMap = restTemplate.getForObject(REST_SERVICE_URI+"/user/bulk/"+count, List.class);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing SelectManyUsers API---END-----"+end+"msec");
    }
    
    /* GET */
    private static void selectUser(int id){
        System.out.println("Testing selectUser API---BEGIN---"+id);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        User user = restTemplate.getForObject(REST_SERVICE_URI+"/user/"+id, User.class);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing SelectUser API---END-----"+end+"msec");
    }
     
    /* POST */
    private static void createUser() {
        System.out.println("Testing createUser API---BEGIN---");
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        User user = new User(1,"Connor",1000);
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/user/", user, User.class);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing createUser API---END-----"+end+"msec");
    }
    private static void createManyUserByOne(int count) {
       System.out.println("Testing createManyUsersByOne API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/user/one/"+count, URI.class);
//        System.out.println("Location : "+uri.toASCIIString());
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing createManyUsersByOne API---END-----"+end+"msec");
    }
    
    private static void createManyUserByBulk(int count) {
        System.out.println("Testing createManyUsersByBULK API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        URI uri = restTemplate.postForLocation(REST_SERVICE_URI+"/user/bulk/"+count, URI.class);
//        System.out.println("Location : "+uri.toASCIIString());
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing createManyUsersByBULK API---END-----"+end+"msec");
    }
 
    /* PUT */
    private static void updateUser(int id) {
        System.out.println("Testing updateUser API---BEGIN---"+id);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        User user  = new User(id,"updatedUser",id);
        restTemplate.put(REST_SERVICE_URI+"/user/"+id, user);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing updateUser API---END-----"+end+"msec");
    }
    
    private static void updateManyUsersByBulk(int count) {
        System.out.println("Testing updateManyUsersByBylk API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.put(REST_SERVICE_URI+"/user/bulk/"+count, count);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing updateManyUsersByBylk API---END-----"+end+"msec");
    }
 
    /* DELETE */
    private static void deleteUser(int id) {
         System.out.println("Testing deleteUser API---BEGIN---"+id);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/user/"+id);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing deleteUser API---END-----"+end+"msec");
    }
 
 
    /* DELETE */
    private static void deleteManyUsersByBulk(int count) {
        System.out.println("Testing deleteManyUsersByBylk API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/user/bulk/"+count);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing deleteManyUsersByBulk API---END-----"+end+"msec");
    }
    
    private static void deleteManyUsersByOne(int count) {
        System.out.println("Testing deleteManyUsersByOne API---BEGIN---"+count);
        Long begin =System.currentTimeMillis();
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(REST_SERVICE_URI+"/user/one/"+count);
        Long end =System.currentTimeMillis()-begin;
        System.out.println("Testing deleteManyUsersByOne API---END-----"+end+"msec");
    }
    
 
    public static void main(String args[]) throws UnknownHostException{
       SpringRestTestClient test = new SpringRestTestClient();
        createUser();        
//        deleteManyUsersByBulk(1000000);
//        createUser();
        createManyUserByOne(100);
        deleteManyUsersByBulk(100);
//         System.out.println("////////////////////////////////////////////");
        createManyUserByOne(500);
        deleteManyUsersByBulk(500);
//        createManyUserByOne(500);
//        deleteManyUsersByOne(500);
//         System.out.println("////////////////////////////////////////////");
//        createManyUserByOne(1000);
//        deleteManyUsersByBulk(1000);
//        createManyUserByOne(1000);
//        deleteManyUsersByOne(1000);
//         System.out.println("////////////////////////////////////////////");
//        createManyUserByOne(5000);
//        deleteManyUsersByBulk(5000);
//        createManyUserByOne(5000);
//        deleteManyUsersByOne(5000);
//         System.out.println("////////////////////////////////////////////");
//        createManyUserByBulk(10000);
//        deleteManyUsersByBulk(10000);
//         System.out.println("////////////////////////////////////////////");
//       createManyUserByBulk(20000);
//        deleteManyUsersByBulk(20000);
//        createManyUserByBulk(50000);
//        deleteManyUsersByBulk(50000);
//         System.out.println("////////////////////////////////////////////");
        createManyUserByBulk(100000);
        deleteManyUsersByBulk(100000);
         System.out.println("////////////////////////////////////////////");
        createManyUserByBulk(1000000);
//        deleteManyUsersByBulk(100000);
        
        updateManyUsersByBulk(100000);
        updateManyUsersByBulk(500000);
        updateManyUsersByBulk(1000000);
        
      System.out.println("////////////////////////////////////////////");
       selectUser(1000);
       selectManyUsers(100);
       selectManyUsers(500);
       selectManyUsers(1000);
       selectManyUsers(5000);
       selectManyUsers(10000);
       selectManyUsers(20000);
       selectManyUsers(50000);
       System.out.println("////////////////////////////////////////////");
    }
}