#!/bin/bash
if [ $# -eq 0 ]; then 
		echo "WARN: Please use command wtih argument:get_jar.sh jar_name"
		exit 1
fi
findobject=$1

echo "Enter your name:"
read username
name_length=${#username}
if [ $name_length -eq 0 ]; then
	echo "WARN: Please re-enter your name:"
	read username
fi
echo "Enter your password:"
read -s pass
. ./artifactory.properties
file_property="./artifactory.properties"
if [ -f "$file_property" ]; then 
		echo "INFO: $file_property found."
		echo "INFO: Server: $http_artifactory_server"
	else 
		echo "WARN: $file_property not found. It need to contain row: http_artifactory_server=http://SERVER_NAME:PORT/artifactory"
		exit 1
fi

findurl=`curl -u $username:$pass -sS $http_artifactory_server/api/search/artifact?name=$findobject&repos=*`
findurl_length=${#findurl}
loginfailed=$(echo $findurl |awk '{print $11 $12}' | cut -d'"' -f 2 ) 

if [ "$loginfailed" = 'Badcredentials' ]; then
	echo "WARN: login or password incorrect"
	exit 1
else 
if [ $findurl_length -lt 25 ]; then
		echo "WARN: $findobject not found"
		exit 1
	else 
		echo "INFO: $findobject found"
fi
fi

geturl=$(echo $findurl |awk '{print $8}' | cut -d'"' -f 2) 

curl -u $username:$pass -O $geturl &>/dev/null
echo "INFO: $findobject downloaded"
mv $findobject /var/lib/
echo "INFO: $findobject in directory /var/lib"